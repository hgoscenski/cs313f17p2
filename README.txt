TestList
    All the tests passed, either as LinkedList or ArrayList- subtly different times for the tests


TestIterator
    The linkedlist works properly and doesn't make a difference in terms of output
        -subtly different times for the tests less than 1ms differences
    If you use list.remove(77) the program does not run as 77 is larger than the size of the list

TestPerformance
    10      =   ArrayList Access, 13ms | LinkedList Add/Remove, 52ms
                LinkedList Access, 14ms   | ArrayList Add/Remove 29ms

    100     =   ArrayList Access, 23ms | LinkedList Add/Remove, 53ms
                LinkedList Access, 44ms   | ArrayList Add/Remove 75ms

    1000    =   ArrayList Access, 23ms | LinkedList Add/Remove, 43ms
                LinkedList Access, 387ms  | ArrayList Add/Remove 211ms

    10000   =   ArrayList Access, 25ms | LinkedList Add/Remove, 44ms
                LinkedList Access, 5135ms | ArrayList Add/Remove 1915ms